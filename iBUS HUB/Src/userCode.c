/*
 * userCode.c
 *
 *  Created on: Jan 29, 2019
 *      Author: Muhammad Abizhar Mul
 */
#include "userCode.h"
int a = 1;
uint8_t dev_addr = 0xEC;

int setup() {
	initiate_periodic_call(&rx2mu_request_call);
	initiate_periodic_call(&mu2fc_send_call);
	initiate_periodic_call(&target_rate);
	initiate_periodic_call(&rpi2mu_request_call);
	initiate_periodic_call(&mu2rpi_send_call);
	initiate_periodic_call(&mu2raspi_fail);

	bmp_init(dev_addr, hi2c1);
	pressure_param = get_pressure_parameter(dev_addr, hi2c1);
	temperature_param = get_temperature_parameter(dev_addr, hi2c1);

	for (int i = 0; i < FIL_SZ*2; i++) {
		temperature = get_temperature(dev_addr, temperature_param, &t_fine, hi2c1);
		pressure = get_pressure(dev_addr, pressure_param, t_fine, hi2c1);
		altitude = get_altitude(pressure, 0);
		altitude_fil = mov_avg(altitude);
		HAL_Delay(5);
	}
	ref_altitude = altitude_fil;

	buzzer_on.data = 0B1001100110011001;
	buzzer_rpi.data = 0B1010100000001111;
	buzzer_rx.data = 0B1111000000010101;

	buzzer_on.cnt = 17;
	buzzer_rpi.cnt = 17;
	buzzer_rx.cnt = 17;

	buzzer_send(&buzzer_on);

	refresh_delayed_call(&control_delay);

	rx_lost_flag = 1;
	rpi_rx_flag = 0;
	debug_tx = 0;
	control_flag = RX; //initiate control as manual
	last_control_flag = RX;
	last_armed = 0;

	i2c_tx_debug = 0;
	i2c_rx_debug = 0;
	return 0;
}

int loop() {
	if (rx2mu_data[4] == 1000 && last_armed == 2000) //reset ketika transisi dari arm ke disarm
		NVIC_SystemReset();
	last_armed = rx2mu_data[4];


	if (rx2mu_data[9] > 1800) {
		rx_OK = rx_missmatch = rx_empty = rx_inv = 0;
		rpi_OK = rpi_missmatch = rpi_empty = rpi_inv = 0;
		target_count = 0;
		debug_tx = 0;
	} else {
		refresh_delayed_call(&control_delay);
		control_flag = RX;
	}

	if (input_changed(rx2mu_data, prev_rx2mu_data, 5)) {
		refresh_delayed_call(&control_delay);
		control_flag = RX;
		for (int i = 0; i < 14; i++)
			prev_rx2mu_data[i] = rx2mu_data[i];
	} else if (delayed_call(&control_delay, CONTROL_DELAY) && rx2mu_data[9] > 1800) {
		control_flag = RPI;
	}

	if (periodic_call(&rx2mu_request_call, 3)) {
		HAL_UART_Receive_DMA(&huart3, rx2mu_buffer, 32);
		HAL_UART_Receive_DMA(&huart2, rpi2mu_buffer, 32);
		rpi_rx_flag = 1;
	}

	if (periodic_call(&mu2raspi_fail, 20)) {
		HAL_UART_Abort(&huart2);
		debug_tx++;
	}

	if (periodic_call(&mu2fc_send_call, 6)) {
		if (rx_lost_flag == 1) {
			if (control_flag == RPI) {
				send_mu2fc(rpi2mu_data, mu2fc_buffer);
			} else
				send_mu2fc(rx2mu_data, mu2fc_buffer);
		}

		temperature = get_temperature(dev_addr, temperature_param, &t_fine,
				hi2c1);
		pressure = get_pressure(dev_addr, pressure_param, t_fine, hi2c1);
		altitude = get_altitude(pressure, ref_altitude);
		altitude_fil = mov_avg(altitude);
	}

	if (periodic_call(&target_rate, 70)) {
		//a+= 1;
		//HAL_GPIO_TogglePin(onBoardLED_GPIO_Port, onBoardLED_Pin);
		target_count++;
	}

	rx_error = rx_inv + rx_missmatch;
	rx_error_perc = ((float) rx_error / (float) rx_OK) * 100;

	rpi_error = rpi_inv + rpi_missmatch;
	rpi_error_perc = ((float) rpi_error / (float) rpi_OK) * 100;

	if (last_control_flag == RX && control_flag == RPI) { //mencari transisi dari mode manual ke auto
		buzzer_send(&buzzer_rpi); //bunyikan buzzer ketika transisi
		last_control_flag = control_flag;
	} else if (last_control_flag == RPI && control_flag == RX) { //mencari transisi dari mode auto ke manual
		buzzer_send(&buzzer_rx); //bunyikan buzzer ketika transisi
		last_control_flag = control_flag;
	}

	buzzer_callback(&buzzer_on);
	buzzer_callback(&buzzer_rpi);
	buzzer_callback(&buzzer_rx);
	return 0;
}

int decode_Ibus(uint8_t *data_in, uint16_t* data_out) {
	uint16_t buffSum = 0xffff, cksum;

	//HAL_UART_Transmit_DMA(&huart1, data_in, 32);
	if (*data_in == HEADER_1 && *(data_in + 1) == HEADER_2) {
		cksum = *(data_in + 31) << 8;
		cksum |= *(data_in + 30);

		for (uint16_t i = 0; i < 30; i++)
			buffSum -= *(data_in + i);

		if (buffSum != cksum) {
			data_in[0] = 0xfd;
			data_in[1] = 0xff;
			return -1; // checksum missmatch
		}

		for (uint16_t i = 0; i < 14; i++) {
			*(data_out + i) = *(data_in + (2 * (i + 1)) + 1) << 8;
			*(data_out + i) |= *(data_in + (2 * (i + 1)));
		}

		data_in[0] = 0xfd;
		data_in[1] = 0xff;
		return 0; // data 0K
	}

	else if (*data_in == 0xfd && *(data_in + 1) == 0xff)
		return -2; // empty data

	else {
		data_in[0] = 0xfd;
		data_in[1] = 0xff;
		return -3; // invalid header
	}

}
int encode_Ibus(uint16_t *data_in, uint8_t* data_out) {
	data_out[0] = HEADER_1;
	data_out[1] = HEADER_2;

	for (uint16_t i = 0; i < 14; i++) {
		data_out[2 * (i + 1) + 1] = data_in[i] >> 8;
		data_out[2 * (i + 1)] = data_in[i] & 255;
	}

	uint16_t buffSum = 0xffff;
	for (uint16_t i = 0; i < 30; i++)
		buffSum -= *(data_out + i);

	data_out[31] = buffSum >> 8;
	data_out[30] = buffSum & 255;
	return 0;
}

int send_mu2fc(uint16_t *channel_data, uint8_t *buffer) {
	encode_Ibus(channel_data, buffer);
	HAL_UART_Transmit_DMA(&huart1, buffer, 32);
	return 0;
}
int send_mu2rpi(uint16_t *channel_data, uint8_t *buffer) {
	channel_data[10] = (int16_t)altitude_fil;
	encode_Ibus(channel_data, buffer);
	HAL_UART_Transmit_DMA(&huart2, buffer, 32);
	return 0;
}

int periodic_call(perio *perio_struct, uint32_t period) {
	if (perio_struct->flag == 2) { // pertama dipanggil
		perio_struct->prev = HAL_GetTick();
		perio_struct->flag = 0;
		return 1;
	}

	if (HAL_GetTick() - perio_struct->prev >= period) { // jika sesuai periode flag = 1
		perio_struct->flag = 1;
		perio_struct->prev += period;
	} else
		perio_struct->flag = 0;

	if (perio_struct->flag == 1) { // setelah eksekusi flag = 0
		perio_struct->flag = 0;
		return 1;
	} else
		perio_struct->flag = 0;

	return 0;
}
int initiate_periodic_call(perio *perio_struct) {
	perio_struct->flag = 2;
	return 0;
}
void refresh_periodic_call(perio *perio_struct) {
	perio_struct->prev = HAL_GetTick();
}
int delayed_call(perio *perio_struct, uint32_t _delay) {
	if (HAL_GetTick() - perio_struct->prev >= _delay) {
		return 1;
	}
	return 0;
}
void refresh_delayed_call(perio *perio_struct) {
	perio_struct->prev = HAL_GetTick();
}

int input_changed(uint16_t *_data, uint16_t *_last_data, uint16_t _thr) {
	if (_data[0] - _last_data[0] >= _thr || _data[0] - _last_data[0] <= -_thr) {
		return 1;
	}

	if (_data[1] - _last_data[1] >= _thr || _data[1] - _last_data[1] <= -_thr) {
		return 1;
	}

	if (_data[3] - _last_data[3] >= _thr || _data[3] - _last_data[3] <= -_thr) {
		return 1;
	}

	if (_data[2] - _last_data[2] >= _thr || _data[2] - _last_data[2] <= -_thr) {
		return 0;
	}
	return 0;
}

void buzzer_send(buzzer* buzzer_data) {
	buzzer_data->cnt = 65535;
	buzzer_data->tim.flag = 2;
}
void buzzer_callback(buzzer* buzzer_data) {
	uint16_t temp_data = buzzer_data->data >> buzzer_data->cnt;

	if (buzzer_data->cnt < 17)
		HAL_GPIO_WritePin(onBoardLED_GPIO_Port, onBoardLED_Pin, temp_data & 1);

	if (periodic_call(&buzzer_data->tim, 70)) {
		buzzer_data->cnt++;
		//HAL_GPIO_TogglePin(onBoardLED_GPIO_Port, onBoardLED_Pin);
		if (buzzer_data->cnt > 17)
			buzzer_data->cnt = 17;
	}

}

///////////////////////////////////////////
void bmp_init(uint8_t dev_addr, I2C_HandleTypeDef hi2c) {
	uint8_t I2C_tx_buff[3];
	I2C_tx_buff[0] = 0xF4;
	I2C_tx_buff[1] = 0x57;
	I2C_tx_buff[2] = 0x1C;
//	I2C_tx_buff[2] = 0x00;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 3, 20);
}

pres_param get_pressure_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[18];
	uint8_t I2C_tx_buff[1];
	uint16_t temp;

	pres_param ret_val;

	I2C_tx_buff[0] = 0x8E;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 18, 100);

	ret_val.dig_P1 = I2C_rx_buff[1] << 8;
	ret_val.dig_P1 |= I2C_rx_buff[0];

	temp = I2C_rx_buff[3] << 8;
	temp |= I2C_rx_buff[2];
	ret_val.dig_P2 = (int16_t) temp;

	temp = I2C_rx_buff[5] << 8;
	temp |= I2C_rx_buff[4];
	ret_val.dig_P3 = (int16_t) temp;

	temp = I2C_rx_buff[7] << 8;
	temp |= I2C_rx_buff[6];
	ret_val.dig_P4 = (int16_t) temp;

	temp = I2C_rx_buff[9] << 8;
	temp |= I2C_rx_buff[8];
	ret_val.dig_P5 = (int16_t) temp;

	temp = I2C_rx_buff[11] << 8;
	temp |= I2C_rx_buff[10];
	ret_val.dig_P6 = (int16_t) temp;

	temp = I2C_rx_buff[13] << 8;
	temp |= I2C_rx_buff[12];
	ret_val.dig_P7 = (int16_t) temp;

	temp = I2C_rx_buff[15] << 8;
	temp |= I2C_rx_buff[14];
	ret_val.dig_P8 = (int16_t) temp;

	temp = I2C_rx_buff[17] << 8;
	temp |= I2C_rx_buff[16];
	ret_val.dig_P9 = (int16_t) temp;

	return ret_val;
}

temp_param get_temperature_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[6];
	uint8_t I2C_tx_buff[1];
	uint16_t temp;

	temp_param ret_val;

	I2C_tx_buff[0] = 0x88;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 6, 100);

	ret_val.dig_T1 = I2C_rx_buff[1] << 8;
	ret_val.dig_T1 |= I2C_rx_buff[0];

	temp = I2C_rx_buff[3] << 8;
	temp |= I2C_rx_buff[2];
	ret_val.dig_T2 = (int16_t) temp;

	temp = I2C_rx_buff[5] << 8;
	temp |= I2C_rx_buff[4];
	ret_val.dig_T3 = (int16_t) temp;

	return ret_val;
}

double get_temperature(uint8_t dev_addr, temp_param temp_param, int32_t* t_fine,
		I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[3];
	uint8_t I2C_tx_buff[1];

	I2C_tx_buff[0] = 0xFA;
	i2c_tx_debug = HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	i2c_rx_debug = HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 3, 20);

	int32_t temp_ADC;

	temp_ADC = I2C_rx_buff[0];
	temp_ADC <<= 8;
	temp_ADC |= I2C_rx_buff[1];
	temp_ADC <<= 8;
	temp_ADC |= I2C_rx_buff[2];
	temp_ADC >>= 4;

	int32_t var1, var2;

	var1 = ((((temp_ADC >> 3) - ((int32_t) temp_param.dig_T1 << 1)))
			* ((int32_t) temp_param.dig_T2)) >> 11;

	var2 = (((((temp_ADC >> 4) - ((int32_t) temp_param.dig_T1))
			* ((temp_ADC >> 4) - ((int32_t) temp_param.dig_T1))) >> 12)
			* ((int32_t) temp_param.dig_T3)) >> 14;

	*t_fine = var1 + var2;

	double temperature;
	temperature = (*t_fine * 5 + 128) >> 8;
	temperature /= 100;

	return temperature;
}

double get_pressure(uint8_t dev_addr, pres_param pres_param, int32_t t_fine,
		I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[3];
	uint8_t I2C_tx_buff[1];

	I2C_tx_buff[0] = 0xF7;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 3, 20);

	int32_t pres_ADC;

	pres_ADC = I2C_rx_buff[0];
	pres_ADC <<= 8;
	pres_ADC |= I2C_rx_buff[1];
	pres_ADC <<= 8;
	pres_ADC |= I2C_rx_buff[2];
	pres_ADC >>= 4;

	int64_t var11, var22, p;

	var11 = ((int64_t) t_fine) - 128000;
	var22 = var11 * var11 * (int64_t) pres_param.dig_P6;
	var22 = var22 + ((var11 * (int64_t) pres_param.dig_P5) << 17);
	var22 = var22 + (((int64_t) pres_param.dig_P4) << 35);
	var11 = ((var11 * var11 * (int64_t) pres_param.dig_P3) >> 8)
			+ ((var11 * (int64_t) pres_param.dig_P2) << 12);
	var11 = (((((int64_t) 1) << 47) + var11)) * ((int64_t) pres_param.dig_P1)
			>> 33;

	double pressure;
	if (var11 != 0) {
		// avoid exception caused by division by zero
		p = 1048576 - pres_ADC;
		p = (((p << 31) - var22) * 3125) / var11;
		var11 = (((int64_t) pres_param.dig_P9) * (p >> 13) * (p >> 13)) >> 25;
		var22 = (((int64_t) pres_param.dig_P8) * p) >> 19;

		p = ((p + var11 + var22) >> 8) + (((int64_t) pres_param.dig_P7) << 4);
		pressure = (double) p / 256;
	}

	else
		pressure = -1.0;
	return pressure;
}

double get_altitude(double pressure, double ref_alt) {
	double altitude;
	altitude = 4433000 * (1.0 - pow((pressure / 100) / 1013.25, 0.1903));
	altitude -= ref_alt;
	return altitude;
}

double mov_avg(double in) {
	static double ar[FIL_SZ];
	static int i = 0;

	ar[i] = in;
	i++;
	if (i >= FIL_SZ)
		i = 0;

	double result = 0;
	for (int j = 0; j < FIL_SZ; j++)
		result += ar[j];

	result /= FIL_SZ;
	return result;
}

///////////////////////////////////////////
